//Составить алгоритм: на входе есть числовой массив, необходимо вывести элементы массива кратные 3

import java.util.Scanner;

class Task3 {
    public static void main(String[] args) {
        boolean isTrue = true;
        Scanner scanner = new Scanner(System.in);
        while (isTrue) {
            try {System.out.println("Введите числовой массив");
            String str = scanner.nextLine();
            String[] strArr = str.split(" ");
            int[] arr = new int[strArr.length];
            for (int i = 0; i < strArr.length; i++) {
                arr[i] = Integer.parseInt(strArr[i]);
                if (arr[i] % 3 == 0) {
                    System.out.print(arr[i] + " " + "\n");
                }
            }
            }
            catch (NumberFormatException e) {
                System.out.println("Вы не ввели массив. Попробуйте снова.");
            }
        }
    }
}
