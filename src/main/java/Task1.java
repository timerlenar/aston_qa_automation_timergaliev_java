//Составить алгоритм: если введенное число больше 7, то вывести “Привет”

import java.util.InputMismatchException;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        boolean isTrue = true;
        Scanner scanner = new Scanner(System.in);
        double x;
        while (isTrue) {
            try {
                System.out.println("Введите любое число");
                x = scanner.nextDouble();
                if (x > 7) {
                    System.out.println("Привет");
                }
            }
            catch (InputMismatchException e) {
                System.out.println("Вы не ввели число. Попробуйте снова.");
                break;
            }
        }
    }
}