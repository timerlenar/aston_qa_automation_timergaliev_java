//Составить алгоритм: если введенное имя совпадает с Вячеслав,
// то вывести “Привет, Вячеслав”, если нет, то вывести "Нет такого имени"

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        boolean isTrue = true;
        Scanner scanner = new Scanner(System.in);
        String b = "Вячеслав";
        while (isTrue) {
            System.out.println("Введите Ваше имя");
            String a = scanner.nextLine();
            if (!(a.toLowerCase().equals(b.toLowerCase()))) {
                System.out.println("Нет такого имени.");
            } else {
                System.out.println("Привет, " + b);
                break;
            }
        }
    }
}